import './App.css';
import DashboardFilters from './components/DashboardFilters';



function App() {
  return (
    <div className="App">
      <h1>Sourcing Optimization</h1>
      <DashboardFilters/>

    </div>
  );
}

export default App;
