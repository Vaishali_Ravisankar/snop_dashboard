import React from 'react';
import { withStyles,makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Collapse } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import TablePagination from "@material-ui/core/TablePagination";
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const useStyles = makeStyles({
  table: {
   width:600,
   margin:'0 auto'
  }
});

const StyledTableCell = withStyles((theme) => ({
  head: {
    color: theme.palette.common.black,
    backgroundColor: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },

}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(category, current, alternative) {
  return { category,current,alternative };
}

const rows = [
  createData('RM', 159,169),
  createData('PM', 237, 270),
  createData('Make', 262, 250),
  createData('Inter SU transport', 305,325),
  createData('SU Pallet', 356, 400),
];

export default function ComparisonTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
          <TableCell >ID</TableCell>
            <TableCell >Category</TableCell>
            <TableCell >Current</TableCell>
            <TableCell >Alternative</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {rows.map((row) => (
            <ExpandingRow row={row} />
          ))}
        </TableBody>
      </Table>
      <TablePage
        component="div"
      />
    </TableContainer>
  );
}
class ExpandingRow extends React.Component {
  state = {
    open: false,
    items: [],
    page: 0,
    rowsPerPage: 2,
    totalRows: 0
  };
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: parseInt(event.target.value, 10) });
    this.setState({ page: 0 });
  };
  handleChangePage = (event, newPage) => {
    console.log(newPage, "newPage");
    this.setState({ page: newPage }, () => {
      this.getItems();
    });
  };

  render() {
    const { row } = this.props;
    const { open } = this.state;

    return (
      <>
        <StyledTableRow  width="600"  margin="0 auto" aria-label="simple table">
          <StyledTableCell key={row.id}>
            <IconButton  margin="0 auto"
              onClick={() => this.setState(({ open }) => ({ open: !open }))}
            >
              {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
            </IconButton>

          </StyledTableCell>
          <StyledTableCell >{row.category}</StyledTableCell>
          <StyledTableCell>{row.current}</StyledTableCell>
          <StyledTableCell>{row.alternative}</StyledTableCell>

        </StyledTableRow>
        <Collapse in={open} component="tr" style={{ display: "block" }}>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>name</TableCell>
                  <TableCell>age</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableCell>shanam</TableCell>
                <TableCell>24</TableCell>
              </TableBody>
            </Table>
          </TableContainer>
        </Collapse>

      </>
    );
  }
}
class TablePage extends React.Component {
  state = {
    items: [],
    page: 0,
    rowsPerPage: 2,
    totalRows: 0
  };
  // getItems() {
  // //  let url = `https://reqres.in/api/users?page=${this.state.page +
  //    // 1}&per_page=${this.state.rowsPerPage}`;
  //   // let url = "https://jsonplaceholder.typicode.com/users";
  //   console.log(url);
  //   fetch(url)
  //     .then(res => res.json())
  //     .then(items => {
  //       console.log(items.data, "items");
  //       this.setState({ items: items.data, totalRows: items.total });
  //     })
  //     .catch(err => console.log(err));
  // }


  updateState = item => {
    const itemIndex = this.state.items.findIndex(data => data.id === item.id);
    const newArray = [
      // destructure all items from beginning to the indexed item
      ...this.state.items.slice(0, itemIndex),
      // add the updated item to the array
      item,
      // add the rest of the items to the array from the index after the replaced item
      ...this.state.items.slice(itemIndex + 1)
    ];
    this.setState({ items: newArray });
  };
  // componentDidMount() {
  //   this.getItems();
  // }
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: parseInt(event.target.value, 10) });
    this.setState({ page: 0 });
  };
  handleChangePage = (event, newPage) => {
    console.log(newPage, "newPage");
    this.setState({ page: newPage }, () => {
      // this.getItems();
    });
  };

  render() {

    return (
      <>
        <TablePagination
          component="div"
          count={this.state.totalRows}
          page={this.state.page}
          onChangePage={this.handleChangePage}
          rowsPerPage={this.state.rowsPerPage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />

      </>
    );
  }
}