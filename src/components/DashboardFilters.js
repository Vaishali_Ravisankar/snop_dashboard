import { SignalCellularNull } from '@material-ui/icons';
import React, { Component } from 'react';
import Select from 'react-select';
import WindowedSelect from "react-windowed-select";
import ComparisonTable from './ComparisonTable';
import Button from '@material-ui/core/Button';
import { components } from "react-select";
import { concat } from "lodash";

class DashboardFilters extends Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            defaultOptionValues: null,
            selectedCategory: null,
            selectedBrandForm: null,
            selectedCorporateBrand: null,
            isLoadingCategoryOption: false,
            isLoadingBrandFormOption: false,
            isLoadingCorporateBrandOption: false,
            showTable: false,
            productSKUList: null,
            menuIsOpen: false,
            brandFormOptions: [],
            brandFormOptionsNew: [],
            total: 9200,
            SKUOptions: [],
            totalSKU: 506071,
            selectedProductSKU: null,
            options: [
                { value: "Household4 care", label: "Household4 care" },
                { value: "Skin care4", label: "Skin4 care" },
                { value: "Hair care4", label: "Hair4 care" },
                { value: "Household1 care", label: "Household5 care" },
                { value: "Skin care1", label: "Skin5 care" },
                { value: "Hair care1", label: "Hair5 care" },
                { value: "Household2 care", label: "Household6 care" },
                { value: "Skin care2", label: "Skin6 care" },
                { value: "Hair care2", label: "Hair6 care" },
                { value: "Household3 care", label: "Household7 care" },
                { value: "Skin care3", label: "Skin7 care" },
                { value: "Hair care3", label: "Hair7 care" },
            ],
        }
    }

    selectCatRef = null;
    selectCorpRef=null;
    selectBrandRef=null;
    selectSKURef=null;



    componentDidMount() {
        fetch('http://localhost:8000/default')
            .then(response => response.json())
            .then(data => {
                console.log(data.Data);
                this.setState({
                    defaultOptionValues: data.Data,
                    loading: false
                })
                this.setOptions(data.Data)

            });
    }

    setOptions = (data) => {
        let initialData = data
        let parentData = initialData ? initialData.BrandFormName : {};
        this.setState({ total: parentData.Count })
        let optionData = (parentData !== null) ? parentData.Data : null;
        let optionValue = (optionData) ? optionData.BrandFormName.map(function (item) {
            return { value: item, label: item };
        }) : {}
        console.log('BrandValue' ,optionValue)
        this.setState({ brandFormOptions: optionValue })

        let SKUParent = initialData ? initialData.SKU : {};
        this.setState({ total: SKUParent.Count })
        let SKUData = SKUParent !== null ? SKUParent.Data : null;
        let SKUValue = (SKUData) ? SKUData.SKU.map(function (item) {
            return { value: item, label: item };
        }) : {}

        console.log('SKUValue', SKUValue)
        this.setState({ SKUOptions: SKUValue })


    }

    searchProduct=(val)=>{
        if(val.length===0){
            const SKUEmptyOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    "CategoryName": this.state.selectedCategory,
                    "BrandFormName": this.state.selectedBrandForm,
                    "CorporateBrandName": this.state.selectedCorporateBrand,
                    "SKU":null
                })
            };
            fetch('http://localhost:8000/product', SKUEmptyOptions)
            .then(response => response.json())
            .then(data => {

                this.setState({
                    defaultOptionValues: data,
                    isLoadingCategoryOption: false,
                    isLoadingBrandFormOption: false,
                    isLoadingCorporateBrandOption: false,
                    isLoadingProductList: false
                })
                this.setOptions(data)
            });
        }
        if(val.length>=3){
            const searchSKUOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    "CategoryName": this.state.selectedCategory,
                    "BrandFormName": this.state.selectedBrandForm,
                    "CorporateBrandName": this.state.selectedCorporateBrand,
                    "SearchChar":val
                })
            };

            fetch('http://localhost:8000/searchsku', searchSKUOptions)
            .then(response => response.json())
            .then(data => {

                this.setState({
                    defaultOptionValues: data,
                    isLoadingCategoryOption: false,
                    isLoadingBrandFormOption: false,
                    isLoadingCorporateBrandOption: false,
                    isLoadingProductList: false
                })
                this.setOptions(data);

                // let brandparent = data ? data.BrandFormName : {};
                // let totalCount = brandparent.Count;
                // this.setState({ total: totalCount })
                // let branddata = (brandparent !== null) ? brandparent.Data : null;
                // let brandvalue = (branddata) ? branddata.BrandFormName.map(function (item) {
                //     return { value: item, label: item };
                // }) : {}
                // this.setState({ brandFormOptions: brandvalue })

                // let brandSKUparent = data ? data.SKU : {};
                // let totalSKUCount = brandSKUparent.Count;
                // this.setState({ totalSKU: totalSKUCount })
                // let brandSKUdata = (brandSKUparent !== null) ? brandSKUparent.Data : null;
                // let brandSKUvalue = (brandSKUdata) ? brandSKUdata.SKU.map(function (item) {
                //     return { value: item, label: item };
                // }) : {}
                // this.setState({ SKUOptions: brandSKUvalue })
            });
        }
    }

    searchBrandNameProduct=(val)=>{
        if(val.length===0){
            const BrandNameEmptyOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    "CategoryName": this.state.selectedCategory,
                    "BrandFormName": null,
                    "CorporateBrandName": this.state.selectedCorporateBrand,
                    "SKU":this.state.selectedProductSKU
                })
            };

            fetch('http://localhost:8000/product', BrandNameEmptyOptions)
            .then(response => response.json())
            .then(data => {

                this.setState({
                    defaultOptionValues: data,
                    isLoadingCategoryOption: false,
                    isLoadingBrandFormOption: false,
                    isLoadingCorporateBrandOption: false,
                    isLoadingProductList: false
                })


                this.setOptions(data)


            });
    

        }
        if(val.length>=3){
            const searchBrandNameOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    "CategoryName": this.state.selectedCategory,
                    "CorporateBrandName": this.state.selectedCorporateBrand,
                    "SKU": this.state.selectedProductSKU,
                    "SearchChar":val
                })
            };

            fetch('http://localhost:8000/searchbrandformname', searchBrandNameOptions)
            .then(response => response.json())
            .then(data => {

                this.setState({
                    defaultOptionValues: data,
                    isLoadingCategoryOption: false,
                    isLoadingBrandFormOption: false,
                    isLoadingCorporateBrandOption: false,
                    isLoadingProductList: false
                })

                this.setOptions(data);


                // let brandparent = data ? data.BrandFormName : {};
                // let totalCount = brandparent.Count;
                // this.setState({ total: totalCount })
                // let branddata = (brandparent !== null) ? brandparent.Data : null;
                // let brandvalue = (branddata) ? branddata.BrandFormName.map(function (item) {
                //     return { value: item, label: item };
                // }) : {}
                // this.setState({ brandFormOptions: brandvalue })

                // let brandSKUparent = data ? data.SKU : {};
                // let totalSKUCount = brandSKUparent.Count;
                // this.setState({ totalSKU: totalSKUCount })
                // let brandSKUdata = (brandSKUparent !== null) ? brandSKUparent.Data : null;
                // let brandSKUvalue = (brandSKUdata) ? brandSKUdata.SKU.map(function (item) {
                //     return { value: item, label: item };
                // }) : {}
                // this.setState({ SKUOptions: brandSKUvalue })



            });
    
        }

    }

    handleCategoryChange = async selectedOption => {
        console.log('option change triggered',selectedOption)
        if(selectedOption !== null){
            await this.setState({ selectedCategory: selectedOption.value });
            console.log(`Option selected:`, selectedOption.value, this.state.selectedCategory);
            const categoryRequestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    "CategoryName": selectedOption.value,
                    "BrandFormName": this.state.selectedBrandForm,
                    "CorporateBrandName": this.state.selectedCorporateBrand,
                    "SKU": this.state.selectedProductSKU
                })
            };
            this.setState({
                isLoadingCategoryOption: true,
                isLoadingBrandFormOption: true,
                isLoadingCorporateBrandOption: true,
                isLoadingProductList: true
            })
            fetch('http://localhost:8000/product', categoryRequestOptions)
                .then(response => response.json())
                .then(data => {
    
                    this.setState({
                        defaultOptionValues: data,
                        isLoadingCategoryOption: false,
                        isLoadingBrandFormOption: false,
                        isLoadingCorporateBrandOption: false,
                        isLoadingProductList: false
                    })
    
    
                    this.setOptions(data)
    
    
                });
        
        }
       };

    handleBrandFormChange = selectedOption => {
        if(selectedOption!==null){
        this.setState({ selectedBrandForm: selectedOption.value });
        console.log(`Option selected:`, selectedOption.value, this.state.selectedBrandForm);
        const brandFormRequestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "CategoryName": this.state.selectedCategory,
                "BrandFormName": selectedOption.value,
                "CorporateBrandName": this.state.selectedCorporateBrand,
                "SKU": this.state.selectedProductSKU
            })
        };
        this.setState({
            isLoadingCategoryOption: true,
            isLoadingBrandFormOption: true,
            isLoadingCorporateBrandOption: true,
            isLoadingProductList: true
        })
        fetch('http://localhost:8000/product', brandFormRequestOptions)
            .then(response => response.json())
            .then(data => {
                console.log('data after post API success', data)
                this.setState({
                    defaultOptionValues: data,
                    isLoadingBrandFormOption: false,
                    isLoadingCategoryOption: false,
                    isLoadingCorporateBrandOption: false,
                    isLoadingProductList: false
                })

                this.setOptions(data)

                //this.setState({brandFormOptions:data.BrandFormName})

                });
        }
    };
    handleCorporateBrandChange = selectedOption => {
        if(selectedOption!==null){
        this.setState({ selectedCorporateBrand: selectedOption.value });
        console.log(`Option selected:`, selectedOption.value, this.state.selectedCorporateBrand);
        const corporateBrandRequestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "CategoryName": this.state.selectedCategory,
                "BrandFormName": this.state.selectedBrandForm,
                "CorporateBrandName": selectedOption.value,
                "SKU": this.state.selectedProductSKU
            })
        };
        this.setState({
            isLoadingCategoryOption: true,
            isLoadingBrandFormOption: true,
            isLoadingCorporateBrandOption: true,
            isLoadingProductList: true
        })
        fetch('http://localhost:8000/product', corporateBrandRequestOptions)
            .then(response => response.json())
            .then(data => {
                console.log('data after post API success', data)
                this.setState({
                    defaultOptionValues: data,
                    isLoadingCorporateBrandOption: false,
                    isLoadingBrandFormOption: false,
                    isLoadingCategoryOption: false,
                    isLoadingProductList: false
                })
                this.setOptions(data)
            });
        }
    };

    handleSKUChange = selectedOption => {
        if(selectedOption!==null){
        this.setState({ selectedProductSKU: selectedOption.value });
        //console.log(`Option selected:`, selectedOption.value, this.state.selectedCorporateBrand);
        const SKURequestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "CategoryName": this.state.selectedCategory,
                "BrandFormName": this.state.selectedBrandForm,
                "CorporateBrandName": this.state.selectedCorporateBrand,
                "SKU": selectedOption.value
            })
        };

        this.setState({
            isLoadingCategoryOption: false,
            isLoadingBrandFormOption: false,
            isLoadingCorporateBrandOption: false,
            isLoadingProductList: false,

        })
    }
    }





    handleReset = e => {
        console.log("reset triggered")
        this.setState({
            defaultOptionValues: null,
            selectedBrandForm: null,
            selectedCategory:null,
            selectedCorporateBrand:null,
            selectedProductSKU:null,
            isLoadingCategoryOption: true,
            isLoadingBrandFormOption: true,
            isLoadingCorporateBrandOption: true,
            isLoadingProductList:true

        })
        this.selectCatRef.select.clearValue();
        this.selectBrandRef.select.clearValue();
        this.selectCorpRef.select.clearValue();
        this.selectSKURef.select.clearValue();
        fetch('http://localhost:8000/default')
            .then(response => response.json())
            .then(data => {
                console.log(data.Data);
                this.setState({
                    defaultOptionValues: data.Data,
                    selectedCategory: null,
                    selectedBrandForm: null,
                    selectedCorporateBrand: null,
                    selectedProductSKU:null,
                    isLoadingCategoryOption: false,
                    isLoadingBrandFormOption: false,
                    isLoadingCorporateBrandOption:false ,
                    isLoadingProductList:false
                })
            });
    }

    handleSearch = () => {
        this.setState({ showTable: true })
        this.setState({
            isLoadingCategoryOption: false,
            isLoadingBrandFormOption: false,
            isLoadingCorporateBrandOption: false,
            isLoadingProductList: false
        })

    }

    addOption = () => {


        const loadMoreBrandFormOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "CategoryName": this.state.selectedCategory,
                "CorporateBrandName": this.state.selectedCorporateBrand,
                "NextRecord": this.state.brandFormOptions.length
            })
        };

        fetch('http://localhost:8000/loadmorebrandformname', loadMoreBrandFormOptions)
            .then(response => response.json())
            .then(data => {
                console.log('data after post API success', data);
                console.log('data', data.BrandFormName)
                let newOptionList = data.BrandFormName;
                let newOptionValue = (newOptionList) ? newOptionList.map(function (item) {
                    return { value: item, label: item };
                }) : {}
                //this.setState({brandFormOptionsNew:newOptionValue})
                const newList = concat(this.state.brandFormOptions, newOptionValue);
                this.setState({ brandFormOptions: newList });
                console.log('New list data after concat', newList)


                // this.setState({
                //     defaultOptionValues: data,
                //     isLoadingCorporateBrandOption: false,
                //     isLoadingBrandFormOption: false,
                //     isLoadingCategoryOption: false,
                //     isLoadingProductList: false
                // })


            });



    };

    addSKUOption = () => {


        const loadMoreSKUOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "CategoryName": this.state.selectedCategory,
                "CorporateBrandName": this.state.selectedCorporateBrand,
                "BrandName": this.state.selectedBrandForm,
                "NextRecord": this.state.SKUOptions.length
            })
        };

        fetch('http://localhost:8000/loadmoresku', loadMoreSKUOptions)
            .then(response => response.json())
            .then(data => {
                console.log('data after post API success', data);
                console.log('data', data.SKU)
                let newSKUOptionList = data.SKU;
                let newSKUOptionValue = (newSKUOptionList) ? newSKUOptionList.map(function (item) {
                    return { value: item, label: item };
                }) : {}
             
                const newSKUList = concat(this.state.SKUOptions, newSKUOptionValue);
                this.setState({ SKUOptions: newSKUList });
                console.log('New list data after concat', newSKUList)


               


            });



    };

    render() {

        console.log('defaultOptionValues', this.state.defaultOptionValues)
        console.log('Selected Values', this.state.selectedBrandForm)
       

        const isProductSKUAvailable = (this.state.productSKUList === null) ? false : true;

        const productSKUListOptionValues = (isProductSKUAvailable) ? this.state.productSKUList.SKU.map(function (item) {
            return { value: item, label: item };
        }) : {}

        console.log("Product List options", productSKUListOptionValues)

        const categoryOptionParent = this.state.defaultOptionValues ? this.state.defaultOptionValues.CategoryName : {};
        const categoryOptionData = (categoryOptionParent !== null) ? categoryOptionParent.Data : null;
        let categoryOptionsValue = (categoryOptionData) ? categoryOptionData.CategoryName.map(function (item) {
            return { value: item, label: item };
        }) : {}
        console.log("Category options", categoryOptionParent, categoryOptionData, categoryOptionsValue);
        console.log('this.state.selectedCategory', this.state.selectedCategory)
      
        const corporateBrandOptionParent = this.state.defaultOptionValues ? this.state.defaultOptionValues.CorporateBrandName : {};
        const corporateBrandOptionData = (corporateBrandOptionParent !== null) ? corporateBrandOptionParent.Data : null;
        let corporateBrandOptionsValue = (corporateBrandOptionData) ? corporateBrandOptionData.CorporateBrandName.map(function (item) {
            return { value: item, label: item };
        }) : {}
       
        const MenuList = (props) => {
            const { MenuListHeader = null, MenuListFooter = null } =
                props.selectProps.components;

            return (
                <components.MenuList {...props}>
                    {props.children.length && MenuListHeader}
                    {props.children}
                    {props.children.length && MenuListFooter}
                </components.MenuList>
            );
        };

        const MenuListFooter = ({ showing, total, onClick }) =>
            !showing || showing >= total ? null : (

                <Button variant="contained" color="primary" href="#contained-buttons" onClick={onClick}>
                    Load More..
                </Button>

            );



        return (

            <React.Fragment>
                {this.state.loading ? (
                    <div className="divLoader">
                        <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                            <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                        </svg>
                    </div>
                ) : (
                    <React.Fragment>
                        <div className="filterContainer">
                            <Select 
                             ref={ref => {
                               this.selectCatRef = ref;
                              }}
                            options={categoryOptionsValue} placeholder="Select Category" onChange={this.handleCategoryChange} isLoading={this.state.isLoadingCategoryOption}
                            />
                            <Select 
                            ref={ref => {
                                this.selectCorpRef = ref;
                               }}
                               options={corporateBrandOptionsValue} placeholder="Select Corp. Brand" onChange={this.handleCorporateBrandChange} isLoading={this.state.isLoadingCorporateBrandOption} />
                            {/* <Select options={brandFormOptionsValue} placeholder="Select Brand Form" onChange={this.handleBrandFormChange} isLoading={this.state.isLoadingBrandFormOption} /> */}
                            <Select
                            ref={ref => {
                                this.selectBrandRef = ref;
                               }}
                                components={{
                                    MenuList,
                                    MenuListFooter: (
                                        <MenuListFooter
                                            showing={this.state.brandFormOptions.length}
                                            onClick={this.addOption}
                                            total={this.state.total}
                                        />
                                    ),
                                }}
                                options={this.state.brandFormOptions}
                                placeholder="Select Brand Form"
                                onChange={this.handleBrandFormChange}
                                onInputChange={this.searchBrandNameProduct}
                                isLoading={this.state.isLoadingBrandFormOption}
                            />

                            <Select
                            ref={ref => {
                                this.selectSKURef = ref;
                               }}
                                components={{
                                    MenuList,
                                    MenuListFooter: (
                                        <MenuListFooter
                                            showing={this.state.SKUOptions.length}
                                            onClick={this.addSKUOption}
                                            total={this.state.totalSKU}
                                        />
                                    ),
                                }}
                                options={this.state.SKUOptions}
                                placeholder="Select SKU"
                                onChange={this.handleSKUChange}
                                onInputChange={this.searchProduct}
                                isLoading={this.state.isLoadingProductList}
                            />
                            {/* {isProductSKUAvailable &&
                                <WindowedSelect options={productSKUListOptionValues} placeholder="Select SKU" onInputChange={this.handleSKUInput} onChange={this.handleSKUChange} noOptionsMessage={() => 'Too many values! Search to narrow down'} isLoading={this.state.isLoadingProductList} />
                            }

                            {!isProductSKUAvailable &&
                                <WindowedSelect placeholder="Select SKU" onInputChange={this.handleSKUInput} noOptionsMessage={() => 'Too many values! Search to narrow down'} isLoading={this.state.isLoadingProductList} />
                            } */}
                            {/* <Select
                                components={{
                                    MenuList,
                                    MenuListFooter: (
                                        <MenuListFooter
                                            showing={this.state.options.length}
                                            onClick={addOption}
                                            total={total}
                                        />
                                    ),
                                }}
                                options={this.state.options}
                            /> */}
                        </div>
                        <div className="ButtonContainer">
                            <Button variant="contained" color="primary" onClick={this.handleReset}>
                                Reset
                            </Button>

                            <Button variant="contained" color="primary" onClick={this.handleSearch}>
                                Search Matches
                            </Button>
                        </div>
                        {this.state.showTable &&
                            <ComparisonTable />
                        }
                    </React.Fragment>
                )

                }
            </React.Fragment>
        );

    }
}
export default DashboardFilters;

